# Extensions

## Theme

Material Theme Icons

Disable Ligatures

## C/C++

C/C++

## Java

Java Extension Pack

## JavaScript

ESLint

## Markdown

Markdownlint

## Python

Python

## Rust

Better TOML

CodeLLDB

crates

rust-analyzer

## Copilot

GitHub Copilot
